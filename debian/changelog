rust-nanorand (0.7.0-12) unstable; urgency=medium

  * stop mention dh-cargo in long description
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + improve usage comment
  * update watch file:
    + improve filename mangling
    + use Github API
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Feb 2025 12:55:06 +0100

rust-nanorand (0.7.0-11) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * reduce autopkgtest to check single-feature tests only on amd64

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 31 Jul 2024 21:34:28 +0200

rust-nanorand (0.7.0-10) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)
  * declare compliance with Debian Policy 4.7.0
  * relax to (build-/autopkgtest-)depend unversioned
    when version is satisfied in Debian stable
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 14:50:34 +0200

rust-nanorand (0.7.0-9) unstable; urgency=medium

  * update dh-cargo fork;
    closes: bug#1047184, thanks to Lucas Nussbaum
  * renumber patch 2002 -> 1002;
    update DEP-3 patch headers

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 14:51:34 +0200

rust-nanorand (0.7.0-8) unstable; urgency=medium

  * add patch 2002 to avoid feature rdseed,
    seemingly broken on 32-bit x86 arch;
    update autopkgtests;
    stop provide feature rdseed

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 05 Feb 2023 18:28:38 +0100

rust-nanorand (0.7.0-7) unstable; urgency=medium

  * provide virtual package for feature zeroize

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Feb 2023 13:05:16 +0100

rust-nanorand (0.7.0-6) unstable; urgency=medium

  * add patch 1001 to add feature constraints to tests

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 03 Feb 2023 14:41:30 +0100

rust-nanorand (0.7.0-5) unstable; urgency=medium

  * tighten autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 18:49:07 +0100

rust-nanorand (0.7.0-4) unstable; urgency=medium

  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * stop superfluously provide
    virtual un- or zero-versioned feature packages
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 09:00:29 +0100

rust-nanorand (0.7.0-3) unstable; urgency=medium

  * fix provide virtual packages for feature getrandom
  * tighten autopkgtests
    to depend on specific virtual feature packages

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 04 Nov 2022 08:56:29 +0100

rust-nanorand (0.7.0-2) unstable; urgency=medium

  * add patch 2001 to avoid getrandom feature js;
    build-depend on librust-getrandom-0.2+default-dev
    librust-getrandom-0.2+rdrand-dev;
    stop depend on librust-getrandom-0.2+js-dev

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 03 Nov 2022 23:42:55 +0100

rust-nanorand (0.7.0-1) experimental; urgency=low

  * initial release;
    closes: bug#1023213

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 31 Oct 2022 19:19:24 +0100
